(defproject sputnik-frontend "0.1.0-SNAPSHOT"
  :description "FIXME: write this!"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/clojurescript "0.0-2655"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]

                 ;; backend
                 [com.stuartsierra/component "0.2.2"]
                 [duct "0.0.3"]
                 [environ "1.0.0"]
                 [compojure "1.2.1"]
                 [meta-merge "0.1.0"]
                 [ring/ring-json "0.3.1"]
                 [ring/ring-defaults "0.1.2"]
                 [ring-jetty-component "0.2.1"]
                 [com.cemerick/friend "0.2.0" :exclusions [ring/ring-core org.clojure/core.cache]]
                 [friend-oauth2 "0.1.1"]
                 [com.novemberain/monger "2.0.0"]
                 [org.danielsz/system "0.1.1"]
                 [prone "0.6.0"]

                 ;; frontend
                 [om "0.7.3"]
                 [prismatic/om-tools "0.3.3" :exclusions [om org.clojure/clojure]]
                 [cljs-http "0.1.16" :exclusions [org.clojure/core.async]]
                 [bootstrap-cljs "0.0.2" :exclusions [org.clojure/clojure]]
                 [quile/component-cljs "0.2.2" :exclusions [org.clojure/clojure]]]

  :plugins [[lein-bower "0.5.1"]
            [quickie "0.3.6"]]
  :bower-dependencies [[bootstrap-material-design "~0.2"]
                       [bootstrap "~3.2.0"]
                       [react "0.11.2"]
                       [toastr "2.1.0"]
                       [react-bootstrap "~0.11"]
                       [es5-shim "4.0.3"]]
  :bower {:directory "resources/public/js/lib"}

  :source-paths ["src/clj"]
  :test-paths ["test/clj"]

  :main ^:skip-aot sputnik-frontend.main

  :cljsbuild {
    :builds [{:id "dev"
              :source-paths ["src/cljs" "env/dev/cljs"]
              :compiler {:output-to "resources/public/js/compiled/sputnik_frontend.js"
                         :output-dir "resources/public/js/compiled/out"
                         :optimizations :none
                         :source-map true}}
             {:id "test"
              :source-paths ["src/cljs" "test/cljs"]
              :notify-command ["phantomjs" "test/bin/runner-none.js"
                               "target/out" "target/testable.js"
                               "resources/public/js/lib/es5-shim/es5-shim.js"
                               "resources/public/js/lib/jquery/dist/jquery.js"
                               "resources/public/js/lib/react/react-with-addons.js"
                               "resources/public/js/lib/bootstrap/dist/js/bootstrap.js"
                               "resources/public/js/lib/react-bootstrap/react-bootstrap.js"]
              :compiler {:output-to     "target/testable.js"
                         :output-dir    "target/out"
                         :optimizations :none
                         :pretty-print  true}}
             {:id "min"
              :source-paths ["src/cljs"]
              :compiler {:output-to "www/sputnik_frontend.min.js"
                         :optimizations :advanced
                         :pretty-print false
                         :preamble ["react/react.min.js"]
                         :externs ["react/externs/react.js"]}}]
    :test-commands {"test" ["phantomjs" "test/bin/runner-none.js"
                            "target/out" "target/testable.js"
                            "resources/public/js/lib/es5-shim/es5-shim.js"
                            "resources/public/js/lib/jquery/dist/jquery.js"
                            "resources/public/js/lib/react/react-with-addons.js"
                            "resources/public/js/lib/bootstrap/dist/js/bootstrap.js"
                            "resources/public/js/lib/react-bootstrap/react-bootstrap.js"]}}

  :aliases {"cljs-auto-test" ["do" "clean," "cljsbuild" "auto" "test"]}

  :profiles {:dev {:source-paths ["env/dev/clj"]
                   :repl-options {:init-ns backend}
                   :dependencies [[figwheel "0.1.4-SNAPSHOT" :exclusions [org.clojure/core.async]]
                                  [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                                  [reloaded.repl "0.1.0"]
                                  [org.clojure/tools.namespace "0.2.4"]
                                  [fongo-component "0.1.0"]
                                  [midje "1.6.3"]
                                  [ring-mock "0.1.5"]
                                  [clj-http-fake "1.0.1"]
                                  [jayq "2.5.2"]]
                   :plugins [[lein-cljsbuild "1.0.4-SNAPSHOT"]
                             [lein-figwheel "0.1.4-SNAPSHOT"]
                             [lein-ring "0.8.13"]
                             [com.cemerick/clojurescript.test "0.3.3"]]

                   :ring {:handler sputnik-frontend.endpoint/app}
                   :figwheel {:http-server-root "public"
                              :server-port 3267
                              :css-dirs ["resources/public/css"]}}})
