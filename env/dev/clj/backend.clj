(ns backend
  (:require [clojure.repl :refer :all]
            [clojure.pprint :refer [pprint]]
            [clojure.tools.namespace.repl :refer [refresh]]
            [clojure.java.io :as io]
            [com.stuartsierra.component :as component]
            [reloaded.repl :refer [system init start stop go reset]]
            [ring.middleware.stacktrace :refer [wrap-stacktrace]]
            [sputnik-frontend.main :as main]
            [sputnik-frontend.system :as system]))

(def config
  {:http {:port 8080}
   :app  {:middleware [wrap-stacktrace]}})

(reloaded.repl/set-init! #(system/new-system (merge config main/config)))
