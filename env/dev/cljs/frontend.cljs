(ns frontend
  (:require [quile.component :as component]
            [sputnik-frontend.core :as core]
            [sputnik-frontend.system :as system]
            [figwheel.client :as figwheel :include-macros true]))

(enable-console-print!)

(defonce app-state (atom {}))

(defonce a-system (component/start (system/new-system {:state app-state})))

(figwheel/watch-and-reload
  :websocket-url "ws://localhost:3267/figwheel-ws"
  :jsload-callback (fn []
                     (println "reloaded")
                     (component/stop a-system)
                     (component/start (system/new-system {:state app-state}))))
