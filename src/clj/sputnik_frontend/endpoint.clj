(ns sputnik-frontend.endpoint
  (:require [sputnik-frontend.repository :as r]
            [clojure.java.io :as io]
            [clojure.set :refer [rename-keys]]
            [compojure.core :refer :all]
            [compojure.handler :as handler]
            [compojure.route :as route]
            [cemerick.friend :as friend]
            [clj-http.client :as client]
            [friend-oauth2.workflow :as oauth2]
            [friend-oauth2.util :refer [format-config-uri get-access-token-from-params]]
            [cheshire.core :as j]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [ring.util.response :refer [resource-response response]]
            (cemerick.friend [workflows :as workflows]
                             [credentials :as creds])))

(declare fetch-and-store-repos)
(declare activate-hook)
(declare deactivate-hook)
(declare github-get-repos)
(declare github-get-login)
(declare fetch-access-token)

(defn credential-fn
  [token]
  ;;lookup token in DB or whatever to fetch appropriate :roles
  {:identity token :roles #{::user}})

(defn client-config [app-config]
  (let [oauth-config (:oauth app-config)
        http-config (:http app-config)]
    {:client-id (:client-id oauth-config)
     :client-secret (:client-secret oauth-config)
     :callback {:domain (str "http://" (:host http-config) ":" (:port http-config))
                :path (:callback-path oauth-config)}}))

(defn uri-config [client-config]
  {:authentication-uri {:url "https://github.com/login/oauth/authorize"
                        :query {:client_id (:client-id client-config)
                                :response_type "code"
                                :redirect_uri (format-config-uri client-config)
                                :scope "repos,admin:repo_hook"}}

   :access-token-uri {:url "https://github.com/login/oauth/access_token"
                      :query {:client_id (:client-id client-config)
                              :client_secret (:client-secret client-config)
                              :grant_type "authorization_code"
                              :redirect_uri (format-config-uri client-config)}}})

(defn app-routes [repo-comp config]
  (routes
    (route/resources "/")
    ;; landing page
    (GET "/" [] (io/resource "site.html"))

    ;; logout
    (friend/logout (ANY "/logout" request (ring.util.response/redirect "/")))

    ;; app
    (GET "/app" request
      (friend/authorize #{::user} (slurp (io/resource "public/index.html"))))

    (GET "/repos" request
      (let [access-token (fetch-access-token request)
            login (github-get-login access-token)]
        (response
          (->>
            (let [stored-repos (r/find-all-by-username repo-comp login)]
              (if (empty? stored-repos)
                (fetch-and-store-repos repo-comp login (fetch-access-token request))
                stored-repos))
            (map #(select-keys % [:id :full-name :username :enabled :settings]))))))

    (POST "/repos/:owner/:repo" {{owner :owner repo :repo} :params {value "value"} :body :as request}
      (let [access-token (fetch-access-token request)
            ;; todo login from access token
            login (github-get-login access-token)]
        (response
          (if (boolean value)
            (activate-hook repo-comp owner repo access-token)
            (deactivate-hook repo-comp owner repo access-token)))))

    (POST "/repos/:owner/:repo/settings" {{owner :owner repo :repo} :params settings :body :as request}
      (let [access-token (fetch-access-token request)
            login (github-get-login access-token)]
        ;; check if owner == login
        (r/save-settings repo-comp (str owner "/" repo) settings)
        (response settings)))))

(defn fetch-and-store-repos [repo-comp username access-token]
  (let [repos-response (github-get-repos access-token)
        repos (->> repos-response
                   (map #(select-keys % [:id :full_name]))
                   (map #(rename-keys % {:full_name :full-name}))
                   (map #(assoc % :username username :enabled false))
                   vec)]
    (r/save-all repo-comp repos)
    repos))

(defn activate-hook [repo-comp owner repo access-token]
  (let [full-name (str owner "/" repo)
        url (str "https://api.github.com/repos/" full-name "/hooks?access_token=" access-token)
        response (client/post url {:form-params {:name "web" :active true :events ["pull_request"]
                                                :config {:url "http://sputnik.touk.pl" :content_type "json"}}
                                   :content-type :json})
        parsed-response (j/parse-string (:body response) true)]
    (r/add-hook repo-comp owner full-name (:id parsed-response))
    (:url parsed-response)))

(defn deactivate-hook [repo-comp owner repo access-token]
  (let [full-name (str owner "/" repo)
        hook-id (:hook-id (r/find-by-full-name repo-comp full-name))
        url (str "https://api.github.com/repos/" owner "/" repo "/hooks/" hook-id "?access_token=" access-token)]
    (r/remove-hook repo-comp owner full-name)
    (client/delete url)))

(defn- fetch-access-token [request]
  (get-in (friend/current-authentication request) [:identity :access-token]))

(defn- github-get-login [access-token]
  (let [url (str "https://api.github.com/user?access_token=" access-token)
        response (client/get url {:accept :json})]
    (:login (j/parse-string (:body response) true))))

(defn- github-get-repos [access-token]
  (let [url (str "https://api.github.com/user/repos?access_token=" access-token)
        response (client/get url {:accept :json})]
    (j/parse-string (:body response) true)))

(defn app [config repository]
  (let [oauth-client-config (client-config config)]
    (handler/site
     (friend/authenticate
      (app-routes repository config)
      {:allow-anon? true
       :login-uri (get-in config [:oauth :callback-path])
       :workflows [(oauth2/workflow
                    {:client-config oauth-client-config
                     :uri-config (uri-config oauth-client-config)
                     :credential-fn credential-fn
                     :access-token-parsefn get-access-token-from-params})]}))))

(defn endpoint [config context]
  (app config (:repository context)))
