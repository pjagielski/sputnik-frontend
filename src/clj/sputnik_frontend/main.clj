(ns sputnik-frontend.main
  (:gen-class)
  (:require [clojure.java.io :as io]
            [clojure.edn :as edn]
            [com.stuartsierra.component :as component]
            [duct.middleware.errors :refer [wrap-hide-errors]]
            [sputnik-frontend.system :refer [new-system]]
            [environ.core :as environ])
  (:import java.io.PushbackReader))

(def config-file (or (environ/env :config) "config.edn"))

(defn read-config-file []
  (try
    (with-open [r (-> config-file io/reader PushbackReader.)]
      (edn/read r))
    (catch Exception _)))

(defonce env (merge (read-config-file) environ/env))

(def config
  {:http {:port (some-> env :port Integer.) :host (env :host)}
   :app  {:middleware [[wrap-hide-errors :internal-error]]}
   :mongo-uri (env :mongo-uri)
   :oauth (env :oauth)})

(defn -main [& args]
  (let [system (new-system config)]
    (println "Starting system")
    (component/start system)))
