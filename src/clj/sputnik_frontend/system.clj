(ns sputnik-frontend.system
  (:require [com.stuartsierra.component :as component]
            [duct.component.endpoint :refer [endpoint-component]]
            [duct.component.handler :refer [handler-component]]
            [duct.middleware.not-found :refer [wrap-not-found]]
            [ring.middleware.json :refer [wrap-json-body wrap-json-response]]
            [prone.middleware :refer [wrap-exceptions]]
            [meta-merge.core :refer [meta-merge]]
            [ring.component.jetty :refer [jetty-server]]
            [ring.middleware.defaults :refer [wrap-defaults api-defaults]]
            [sputnik-frontend.endpoint :refer [endpoint]]
            [sputnik-frontend.repository :refer [mongo-repositories]]
            [system.components.mongo :refer [new-mongo-db]]))

(def base-config
  {:http {:host "localhost" :port 8080}
   :app  {:middleware [[wrap-not-found :not-found]
                       [wrap-json-body {:keywords? true}]
                       [wrap-json-response]
                       [wrap-exceptions]]}
   :oauth {:client-id "" :client-secret "" :callback-path "/github.callback"}
   :mongo-uri "mongodb://localhost/sputnik"})

(defn new-system [config]
  (let [config (meta-merge base-config config)]
    (-> (component/system-map
         :app        (handler-component (:app config))
         :http       (jetty-server (:http config))
         :endpoint   (endpoint-component (partial endpoint config))
         :repository (mongo-repositories)
         :mongo-db   (new-mongo-db (:mongo-uri config)))
        (component/system-using
         {:http [:app]
          :app [:endpoint]
          :endpoint [:repository]
          :repository [:mongo-db]}))))
