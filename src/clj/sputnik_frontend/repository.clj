(ns sputnik-frontend.repository
  (:refer-clojure :exclude [sort find])
  (:require [com.stuartsierra.component :as component]
            [monger.collection :as mc]
            [monger.operators :refer :all]
            [monger.query :refer :all]
            [monger.json]))

(defprotocol Repositories
  (find-all-by-username [this username])
  (find-by-full-name [this full-name])
  (save-all [this repos])
  (add-hook [this username full-name hook-id])
  (remove-hook [this username full-name])
  (save-settings [this full-name settings]))

(def coll "repositories")

(defrecord MongoRepositories [mongo-db]
  component/Lifecycle Repositories

  (start [component]
    (println "Starting MongoRepositories")
    component)

  (stop [component]
    (println "Stopping MongoRepositories"))

  (find-all-by-username [_ username]
    (with-collection (:db mongo-db) coll
      (find {:username username})
      (sort (array-map :full-name (int 1)))
      (limit 50)))

  (find-by-full-name [_ full-name]
    (mc/find-one-as-map (:db mongo-db) coll {:full-name full-name}))

  (save-all [_ repos]
    (mc/insert-batch (:db mongo-db) coll repos))

  (add-hook [_ username full-name hook-id]
    (mc/update (:db mongo-db) coll {:username username :full-name full-name} {$set {:hook-id hook-id :enabled true}}))

  (remove-hook [_ username full-name]
    (mc/update (:db mongo-db) coll {:username username :full-name full-name} {$unset {:hook-id 1} $set {:enabled false}}))

  (save-settings [_ full-name settings]
    (mc/update (:db mongo-db) coll {:full-name full-name} {$set {:settings settings}})))

(defn mongo-repositories []
  (->MongoRepositories {}))
