(ns sputnik-frontend.system
  (:require [quile.component :as component]
            [sputnik-frontend.core :as core]
            [sputnik-frontend.api :as api]))

(def defaults
  {:api {:base-url "http://localhost:8080"}
   :state (atom {:repos []})
   :node (. js/document (getElementById "app"))})

(defn new-system [config]
  (let [config (merge defaults config)]
    (-> (component/system-map
          :api (api/api-component (:api config))
          :app (core/app-component config))
        (component/system-using
          {:app [:api]}))))
