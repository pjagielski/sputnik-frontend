(ns sputnik-frontend.api
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [cljs.core.async :refer [<! >! chan]]
            [cljs-http.client :as http]
            [quile.component :as component]))

(defprotocol Api
  (fetch-repos [this])
  (toggle-repo [this full-name new-value])
  (save-settings [this full-name new-settings]))

(defrecord ApiComponent [conf]
  component/Lifecycle
  (start [this]
    (assoc this ::started? true))
  (stop [this]
    (assoc this ::started? false))

  Api
  (fetch-repos [this]
    (go (:body (<! (http/get (str (:base-url conf) "/repos"))))))
  (toggle-repo [this full-name new-value]
    (go (<! (http/post (str (:base-url conf) "/repos/" full-name) {:json-params {:value new-value}}))))
  (save-settings [this full-name new-settings]
    (go (<! (http/post (str (:base-url conf) "/repos/" full-name "/settings") {:json-params new-settings})))))

(defn api-component [api-conf]
  (map->ApiComponent {:conf api-conf}))
