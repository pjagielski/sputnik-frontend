(ns sputnik-frontend.core
  (:require-macros [cljs.core.async.macros :refer (go go-loop)])
  (:require [om.core :as om :include-macros true]
            [om-tools.dom :as dom :include-macros true]
            [om-tools.core :refer-macros [defcomponent]]
            [bootstrap-cljs :as bs :include-macros true]
            [cljs.core.async :refer [<! >! chan onto-chan put! alts! timeout]]
            [cljs-http.client :as http]
            [quile.component :as component]
            [sputnik-frontend.api :as api]))

(defn toggle-repo [repo-data owner]
  (let [input (om/get-node owner "toggler")
        new-value (-> input .-checked)]
    (om/set-state! owner :loading true)
    (let [resp-chn (api/toggle-repo (:api (om/get-shared owner)) (:full-name @repo-data) new-value)]
      (go-loop []
        (let [[resp c] (alts! [resp-chn (timeout 500)])]
          (if (= c resp-chn)
            (do
              (om/set-state! owner :loading false)
              (condp = (:status resp)
                200 (do
                      (om/update! repo-data [:enabled] new-value)
                      true)
                500 (do
                      (put! (:toastr (om/get-shared owner)) {:type :error :msg "failed"})
                      false)))
            (do
              (om/update-state! owner :loading not)
              (recur))))))))

(defn check
  [owner {:keys [key label] :as options}]
  (dom/div {:className "checkbox"}
    (dom/label
      (dom/input {:type "checkbox"
                  :on-change #(om/update-state! owner key not)
                  :checked (om/get-state owner key)})
      (dom/span {:className "ripple"})
      (dom/span {:className "check"})
      (dom/div {:className "checkbox-label"}
               label))))

(def settings {:checkstyle "checkstyle" :findbugs "findbugs" :pmd "pmd" :scalastyle "scalastyle" :codenarc "codenarc"})

(defcomponent settings-modal [data owner]
  (init-state [_]
    (let [defaults (into {:visible? false} (map (fn [x] {x false}) (keys settings)))]
      (merge defaults (:settings data))))
  (render-state [_ {:keys [visible?]}]
    (dom/div {:className "pull-right"}
     (bs/button {:on-click #(om/set-state! owner :visible? true)}
                (dom/i {:className "mdi-action-settings"}))
     (when visible?
       (bs/modal {:title (str (:full-name data) " checks")
                  :animated false
                  :on-request-hide #(om/set-state! owner :visible? false)}
                 (dom/div {:class "modal-body"}
                          (for [[key label] settings]
                            (check owner {:key key :label label})))
                 (dom/div {:class "modal-footer"}
                          (bs/button {:on-click #(om/set-state! owner :visible? false)}
                                     "Cancel")
                          (bs/button {:bs-style "primary"
                                      :on-click (fn [_]
                                                  (om/set-state! owner :visible? false)
                                                  (api/save-settings (:api (om/get-shared owner))
                                                                     (:full-name @data)
                                                                     (select-keys (om/get-state owner) (keys settings))))}
                                     "Save")))))))

(defn loading? [owner]
  (om/get-state owner :loading))

(defcomponent repo [repo-data owner]
  (render [_]
    (dom/li {:className "repo"}
      (om/build settings-modal repo-data)
      (dom/div {:className "form-group"}
       (dom/div {:className "checkbox"}
        (dom/label
         (dom/input {:type "checkbox"
                     :on-change #(toggle-repo repo-data owner)
                     :checked (:enabled repo-data)
                     :ref "toggler"})
         (dom/span {:className (if (loading? owner) "hidden" "ripple")})
         (dom/span {:className "check"})
         (dom/div {:className "checkbox-label"}
                  (:full-name repo-data))))))))

(defcomponent repo-list [{:keys [repos]}]
  (render [_]
    (dom/form {:className "repoList form-horizontal"}
      (dom/ul
        (om/build-all repo repos)))))

(defcomponent repo-box [app owner]
  (will-mount [_]
    (when (empty? (:repos app))
      (let [api (:api (om/get-shared owner))]
        (om/set-state! owner :loading true)
        (go (let [repos (<! (api/fetch-repos api))]
              (om/transact! app :repos (fn [_] repos))
              (om/set-state! owner :loading false))))))
  (render [_]
    (dom/div {:className "repoBox well bs-component"}
      (dom/span {:style (when-not (loading? owner) {:display :none})}
                "loading...")
        (om/build repo-list app))))

(defcomponent root-app [app owner]
  (render [_]
    (dom/div
      (bs/navbar
        (dom/div
          (dom/a {:className "navbar-brand"} "Sputnik"))
        (bs/nav {:class "navbar-right account"}
          (bs/dropdown-button {:key 1 :title "Account"}
            (bs/menu-item {:key 1} "Profile")
            (bs/menu-item {:divider true})
            (bs/menu-item {:key 2 :href "/logout"} "Logout"))))
      (om/build repo-box app))))

(defn main [api state toastr node]
  (om/root root-app state
           {:shared {:toastr toastr
                     :api api}
            :target node}))

(defrecord AppComponent [api state node]
  component/Lifecycle
  (start [component]
         (println "Starting app")
         (let [toastr (chan)]
           (go-loop []
                    (let [evt (<! toastr)]
                      (condp = (:type evt)
                        :success (. js/toastr (success (:msg evt)))
                        :error (. js/toastr (error (:msg evt))))
                      (recur)))
           (assoc component :root (main api state toastr node)))
         (println "Started app")
         component)
  (stop [component]
        (dissoc component :root)
        component))

(defn app-component [{:keys [state node]}]
  (map->AppComponent {:state state
                      :node node}))
