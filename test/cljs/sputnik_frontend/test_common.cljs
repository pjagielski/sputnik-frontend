(ns sputnik-frontend.test-common
  (:require-macros [cljs.core.async.macros :refer [go-loop]])
  (:require [cljs.core.async :refer [<! timeout]]))

(defn wait-for
  [callback]
  (go-loop [x 0]
           (<! (timeout 100))
           (if (callback)
             true
             (if (< x 10)
               (recur (inc x))
               false))))

(defn container-div
  "Creates div with a unique id and returns a vector of that new div element
  and id."
  []
  (let [id  (str "container-" (gensym))
        div (.createElement js/document "div")]
    (aset div "id" id)
    [div (str  id)]))

(defn append-container
  "Appends a given container to html's body."
  [container]
  (-> js/document .-body (.appendChild container)))

(defn create-container
  "Creates and returns new container."
  []
  (let [[n id] (container-div)]
    (append-container n)
    (.getElementById js/document id)))

(defn simulate
  "Simulate event on node"
  [node event & args]
  (apply (aget js/React.addons.TestUtils.Simulate (name event))
         node args))
