(ns sputnik-frontend.core-test
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require [cemerick.cljs.test :refer-macros [deftest testing is done]]
            [cljs.core.async :refer [<! timeout]]
            [sputnik-frontend.core :as f]
            [sputnik-frontend.api :as api]
            [sputnik-frontend.test-common :as c]
            [om.core :as om :include-macros true]
            [cljs-http.client :as http]
            [jayq.core :refer [$]]))

(deftest ^:async repo-list
  (go
   (with-redefs [http/get (fn [url]
                            (go (when (= url "http://localhost:8989/repos")
                                  {:body [{:full-name "test/test" :enabled false}]})))
                 http/post (fn [url _]
                             (go (when (= url "http://localhost:8989/repos/test/test")
                                   {:body "ok" :status 200})))]
     (let [cnt (c/create-container)
           app-state (atom {:repos []})
           api (api/api-component {:base-url "http://localhost:8989"})
           $c ($ cnt)]
       (om/root f/repo-box app-state {:target cnt
                                      :shared {:api api}})
       (is (true? (<! (c/wait-for #(= 1 (.-length (.find $c "li")))))))
       (.click (.find $c "li div.checkbox input"))
       (done)))))

(deftest ^:async repo-settings
  (go
   (let [cnt (c/create-container)
         app-state (atom {:full-name "test/test" :enabled false})
         api (api/api-component {:base-url "http://localhost:8989"})
         $c ($ cnt)]
     (om/root f/repo app-state {:target cnt
                                :shared {:api api}})
     (let [$r (.find $c "li")]
       (.click (.find $r "div button"))
       (is (true? (<! (c/wait-for #(= 1 (.-length (.find $r "div.modal-dialog:visible")))))))
       (let [$m (.find $r "div.modal-content")]
         (.click (.find $m "div.checkbox:nth-child(1) input"))
         (.click (.find $m "button:nth-child(2)"))
         (done))))))

