(ns sputnik-frontend.system-test
  (:require [cemerick.cljs.test :refer-macros [deftest testing is done]]
            [quile.component :as component]
            [sputnik-frontend.core :as f]
            [sputnik-frontend.system :as s]
            [sputnik-frontend.api :as api]
            [sputnik-frontend.test-common :as c]
            [om.core :as om :include-macros true]
            [cljs-http.client :as http]
            [jayq.core :refer [$]]))

(deftest new-system
  (let [el (c/create-container)
        system (component/start (s/new-system {:node el}))]
    (is (not (nil? (:app system))))
    (is (not (nil? (:api system))))))
