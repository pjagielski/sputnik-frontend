(ns sputnik-frontend.endpoint-test
  (:use [clojure.test]
        [peridot.core])
  (:require [com.stuartsierra.component :as component]
            [duct.component.endpoint :refer [endpoint-component]]
            [duct.component.handler :refer [handler-component]]
            [ring.middleware.json :refer [wrap-json-body wrap-json-response]]
            [clojure.test :refer :all]
            [sputnik-frontend.endpoint :as e]
            [sputnik-frontend.repository :as r]
            [ring.mock.request :as mock]
            [clj-http.fake :refer :all]
            [cheshire.core :as j]
            [component.fongo :refer [new-fongo]]))

(def config
  {:app {:middleware [[wrap-json-body {:keywords? true}]
                      [wrap-json-response]]}})

(def system
  (->
    (component/system-map
       :app (handler-component (:app config))
       :mongo-db (new-fongo "test")
       :repository (r/mongo-repositories)
       :endpoint (endpoint-component (partial e/endpoint config)))
    (component/system-using
      {:endpoint [:repository]
       :repository [:mongo-db]
       :app [:endpoint]})))

(defn system-fixture [f]
  (alter-var-root #'system component/start)
  (is (not (nil? (:routes (:endpoint system)))))
  (f)
  (alter-var-root #'system component/stop)
  (is (nil? (:routes (:endpoint system)))))

(use-fixtures :once system-fixture)

(deftest test-routes
  (testing "get repositories"
    (let [handler (:routes (:endpoint system))]
      (with-fake-routes
        {{:address "https://api.github.com/user/repos"
          :query-params {:access_token nil}}
          (fn [request] {:status 200 :headers {} :body (j/encode [{:id 1 :full_name "test/test" :not_used 1}])})
         {:address "https://api.github.com/user"
          :query-params {:access_token nil}}
          (fn [request] {:status 200 :headers {} :body (j/encode {:login "test"})})}
        (let [response (handler (mock/request :get "/repos"))]
          (is (= (:status response) 200))
          (is (= (:body response) [{:id 1 :full-name "test/test" :username "test" :enabled false}])))
        ;; second call from mongo
        (let [response (handler (mock/request :get "/repos"))]
          (is (= (:status response) 200))
          (is (= (:body response) [{:id 2 :full-name "test/test" :username "test" :enabled false}]))))))

  (testing "adding hooks"
    (let [handler (:handler (:app system))]
      (r/save-all (:repository system) [{:id 2 :username "hook" :full-name "hook/test"}])
      (with-fake-routes
        {{:address "https://api.github.com/user"
          :query-params {:access_token nil}}
          (fn [request] {:status 200 :headers {} :body (j/encode {:login "test"})})
         {:address "https://api.github.com/repos/hook/test/hooks"
          :request-method :post
          :query-params {:access_token nil}}
          (fn [request] {:status 200 :headers {} :body (j/encode {:id 123 :url "some_url"})})}
        (let [response (-> (session handler)
                           (content-type "application/json")
                           (request "/repos/hook/test" :request-method :post
                                                       :body (j/encode {"value" true})))]
          (is (= (:status (:response response)) 200)))))))
