(ns sputnik-frontend.repository-test
  (:require [com.stuartsierra.component :as component]
            [clojure.test :refer :all]
            [sputnik-frontend.repository :as r]
            [component.fongo :refer [new-fongo]]))

(def system
  (->
    (component/system-map
       :mongo-db (new-fongo "test")
       :repo     (r/mongo-repositories))
    (component/system-using
       {:repo [:mongo-db]})))

(defn mongo-fixture [f]
  (alter-var-root #'system component/start)
  (is (not (nil? (:repo system))))
  (let [repo-list [{:username "test" :full-name "test/repo"}
                   {:username "another" :full-name "another/repo"}]]
    (r/save-all (:repo system) repo-list))
  (f)
  (alter-var-root #'system component/stop)
  (is (nil? (:repo system))))

(use-fixtures :once mongo-fixture)

(deftest mongo-repositories
  (testing "saving and getting object"
    (let [repo (:repo system)]
      (is (not (nil? (r/find-by-full-name repo "test/repo"))))
      (let [found (r/find-all-by-username repo "test")]
        (is (= 1 (count found)))
        (is (= "test/repo" (:full-name (first found)))))))

  (testing "adding and removing hook"
    (let [repo (:repo system)]
      (r/add-hook repo "test" "test/repo" 12345)
      (let [found (r/find-by-full-name repo "test/repo")]
        (is (true? (:enabled found)))
        (is (= 12345 (:hook-id found))))
      (r/remove-hook repo "test" "test/repo")
      (let [found (r/find-by-full-name repo "test/repo")]
        (is (false? (:enabled found)))
        (is (nil? (:hook-id found))))))

  (testing "saving settings"
    (let [repo (:repo system)]
      (r/save-settings repo "test/repo" {:checkstyle true})
      (let [found (r/find-by-full-name repo "test/repo")]
        (is (= {:checkstyle true} (:settings found)))))))
